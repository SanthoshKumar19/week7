package com.hcl.pojo;

public class Contacts {
	private String name;
	private long number;
	private long altNumber;
	private String email;
	private String address;
	public Contacts() {
		super();
	}
	public Contacts(String name, long number, long altNumber, String email, String address) {
		super();
		this.name = name;
		this.number = number;
		this.altNumber = altNumber;
		this.email=email;
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	public long getAltNumber() {
		return altNumber;
	}
	public void setAltNumber(long altNumber) {
		this.altNumber = altNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Contacts [name=" + name + ", number=" + number + ", altNumber=" + altNumber + ", email=" + email
				+ ", address=" + address + "]";
	}
	

}
