package com.hcl.lab1;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import com.hcl.pojo.Contacts;
import com.hcl.service.ContactServiceImp;

/**
 * Servlet implementation class ContactApp
 */
public class ContactApp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ContactApp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ContactServiceImp serviceImp=new ContactServiceImp();
		PrintWriter pen=response.getWriter();
		String name=request.getParameter("name");

		long num=Long.parseLong(request.getParameter("num"));
		long altNum=Long.parseLong(request.getParameter("altNum"));
		String mail=request.getParameter("mail");
		String address=request.getParameter("add");
		Contacts now=new Contacts(name,num,altNum,mail,address);
		if(serviceImp.addContact(now)==1) {
			pen.print("Contact added successfully");
		}
	
				
		
	
	}

}
