package com.hcl.lab1;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.hcl.pojo.Contacts;
import com.hcl.service.ContactServiceImp;

/**
 * Servlet implementation class ShowContact
 */
public class ShowContact extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowContact() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ContactServiceImp serviceImp=new ContactServiceImp();
		List<Contacts> showList=serviceImp.showContacts();
		PrintWriter pen=response.getWriter();
		for(Contacts c:showList) {
			pen.println(c.getName());
			pen.println(c.getNumber());
			pen.println(c.getAltNumber());
			pen.println(c.getEmail());
			pen.println(c.getAddress());
			pen.println();
			}
		pen.println("Thank you for using our application");
		
//		pen.print(showList.size());
	}

}
