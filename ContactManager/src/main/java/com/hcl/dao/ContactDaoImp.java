package com.hcl.dao;

import java.util.ArrayList;
import java.util.List;

import com.hcl.pojo.Contacts;

public class ContactDaoImp implements IContactDao {
static List<Contacts> contactList=new ArrayList<Contacts>();

	@Override
	public int addContact(Contacts contact) {
		int res=0;
		if(contactList.add(contact)) {
			res=1;
		}
		else {
			res=0;
		}
		
		return res;
	}

	@Override
	public List<Contacts> showContacts() {
		
	
		return contactList;
	}

}
