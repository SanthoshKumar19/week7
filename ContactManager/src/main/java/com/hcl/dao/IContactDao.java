package com.hcl.dao;

import java.util.List;

import com.hcl.pojo.Contacts;

public interface IContactDao {
	public int addContact(Contacts contact);
	public List<Contacts> showContacts();
}
