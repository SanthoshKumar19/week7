package com.hcl.service;

import java.util.List;

import com.hcl.pojo.Contacts;

public interface IContactService {
	
	public int addContact(Contacts contact);
	public List<Contacts> showContacts();

}
