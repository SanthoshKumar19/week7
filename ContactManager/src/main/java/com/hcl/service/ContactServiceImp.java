package com.hcl.service;

import java.util.List;

import com.hcl.dao.ContactDaoImp;
import com.hcl.pojo.Contacts;

public class ContactServiceImp implements IContactService {
	ContactDaoImp dao=new ContactDaoImp();
	@Override
	public int addContact(Contacts contact) {
		
		return dao.addContact(contact);
	}

	@Override
	public List<Contacts> showContacts() {
		// TODO Auto-generated method stub
		return dao.showContacts();
	}

}
